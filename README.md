# BasePNG Encoder

BasePNG encoder is a simple file to image encoder I made because I was bored.

Have you ever wanted to see a visual representation of a file?

No?

Well, now you can!

## Usage

 - _Encoding a file:_ `java -jar BasePNG.jar encode <input> <output>`
 
 - _Decoding a file:_ `java -jar BasePNG.jar decode <input> <output>`
 
## Trivia
The image `README.png` in this repository is the image-encoding of this README.md file.

Try decoding the image with the command `java -jar BasePNG.jar decode README.png README.md2`

The MD5 hashes of README.md and README.md2 should be equal!