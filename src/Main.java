import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

	public static void main(String[] Arguments)
	{
		if (Arguments.length != 3)
			System.err.println("Usage: <encode/decode> inputPath outputPath");
		else if (!new File(Arguments[1]).exists())
			System.err.println("File not found: " + Arguments[1]);
		else
			switch (Arguments[0])
			{
				case "encode":
					Main.Encode(Arguments[1], Arguments[2]);
					break;
				case "decode":
					Main.Decode(Arguments[1], Arguments[2]);
					break;
				default:
					System.err.println("Argument 1 must be \"encode\" or \"decode\"!");
			}
	}

	private static void Encode(final String InputPath, final String OutputPath)
	{
		File InputFile = new File(InputPath);
		int FileSize = (int) InputFile.length();
		int TotalPixels = (int) Math.ceil((double) FileSize / 3D);
		int Width = (int) Math.ceil(Math.sqrt((double) TotalPixels));
		int Height = (int) Math.ceil((double) TotalPixels / (double) Width);
		BufferedImage Image = new BufferedImage(Width, Height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D Graphics = Image.createGraphics();
		int X = 0, Y = 0;
		byte[] Bytes = new byte[3];
		try (FileInputStream Input = new FileInputStream(InputFile))
		{
			int BytesRead;
			while ((BytesRead = Input.read(Bytes)) != -1)
				if (BytesRead == 3)
				{
					int R = Bytes[0] + 128;
					int G = Bytes[1] + 128;
					int B = Bytes[2] + 128;
					Graphics.setColor(new Color(R, G, B, 255));
					Graphics.fillRect(X, Y, 1, 1);
					++X;
					if (X >= Width)
					{
						X = 0;
						++Y;
					}
				}
				else
				{
					System.out.println("Bytes left: " + BytesRead);
					if (BytesRead > 0)
					{
						Graphics.setColor(new Color(127, 0, 0, Bytes[0] + 128));
						Graphics.fillRect(X, Y, 1, 1);
						++X;
					}
					if (BytesRead > 1)
					{
						Graphics.setColor(new Color(255, 0, 0, Bytes[1] + 128));
						Graphics.fillRect(X, Y, 1, 1);
					}
				}
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
		Graphics.dispose();
		System.out.println("Total byte amount: " + FileSize);
		try
		{
			ImageIO.write(Image, "png", new File(OutputPath));
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private static void Decode(final String InputPath, final String OutputPath)
	{
		try
		{
			BufferedImage Image = ImageIO.read(new File(InputPath));
			FileOutputStream Output = new FileOutputStream(new File(OutputPath));
			int X = 0, Y = 0, TotalByteCount = 0;
			WritableRaster Raster = Image.getRaster();
			while (X < Image.getWidth() && Y < Image.getHeight())
			{
				int[] Pixel = new int[4];
				Raster.getPixel(X, Y, Pixel);
				if (Pixel[3] == 255)
				{
					Output.write(Pixel[0] - 128);
					Output.write(Pixel[1] - 128);
					Output.write(Pixel[2] - 128);
					TotalByteCount += 3;
				}
				else if (Pixel[0] == 127 || Pixel[0] == 128 || Pixel[0] == 255)
				{
					Output.write(Pixel[3] - 128);
					System.out.println("Rogue byte found!");
					++TotalByteCount;
				}
				else
					break;
				++X;
				if (X >= Image.getWidth())
				{
					X = 0;
					++Y;
				}
			}
			Output.close();
			System.out.println("Total read bytes: " + TotalByteCount);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}
}